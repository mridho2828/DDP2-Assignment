import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    private static int currentSize = 0;
    private static TrainCar lastTrain;

    public static void addTrain(String name, double weight, double length) {
    	WildCat cat = new WildCat(name, weight, length);
    	TrainCar train;
    	if (lastTrain == null) train = new TrainCar(cat);
    	else train = new TrainCar(cat, lastTrain);
    	++currentSize;
    	lastTrain = train;
    }

    public static String getStatus(double massIndexAverage) {
    	if (massIndexAverage < 18.5) return "underweight";
    	else if (massIndexAverage < 25) return "normal";
    	else if (massIndexAverage < 30) return "overweight";
    	else return "obese";
    }

    public static void departAll() {
    	System.out.println("The train departs to Javari Park");
    	System.out.print("[LOCO]<--"); lastTrain.printCar(); System.out.print("\n");
    	double avg = lastTrain.computeTotalMassIndex() / currentSize;
    	System.out.println("Average mass index of all cats: " + avg);
    	System.out.println("In average, the cats in the train are *" + getStatus(avg) + "*");
    	currentSize = 0;
    	lastTrain = null;
    }

    public static void main(String[] args) {
    	Scanner input = new Scanner(System.in);
    	int n = Integer.parseInt(input.nextLine());
    	for (int i = 0; i < n; ++i) {
    		String[] splitted = input.nextLine().split(",");
    		String name = splitted[0];
    		double weight = Double.parseDouble(splitted[1]);
    		double length = Double.parseDouble(splitted[2]);
    		addTrain(name, weight, length);
    		if (lastTrain.computeTotalWeight() > THRESHOLD) departAll();
    	}
    	if (lastTrain != null) departAll();
    }
}