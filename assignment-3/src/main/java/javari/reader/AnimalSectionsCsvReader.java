package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;
import javari.animal.aves.*;
import javari.animal.mammal.*;
import javari.animal.reptile.*;

public class AnimalSectionsCsvReader extends CsvReader {

	public AnimalSectionsCsvReader(Path file) throws IOException {
		super(file);
	}

	public long countValidRecords() {
		return 3;
	}

	public long countInvalidRecords() {
		return 0;
	}
}