package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;
import javari.animal.aves.*;
import javari.animal.mammal.*;
import javari.animal.reptile.*;

public class AnimalCategoriesCsvReader extends CsvReader {

	public AnimalCategoriesCsvReader(Path file) throws IOException {
		super(file);
	}

	private boolean isValid(String str) {
		String splitted[] = str.split(",");
		if (splitted.length != 3) return false;
		if (splitted[0].equalsIgnoreCase("Cat") || splitted[0].equalsIgnoreCase("Lion") ||
			splitted[0].equalsIgnoreCase("Hamster") || splitted[0].equalsIgnoreCase("Whale")) {
			return splitted[1].equalsIgnoreCase("mammals") && splitted[2].equalsIgnoreCase("Explore the Mammals");
		}
		else if (splitted[0].equalsIgnoreCase("Eagle") || splitted[0].equalsIgnoreCase("Parrot")) {
			return splitted[1].equalsIgnoreCase("aves") && splitted[2].equalsIgnoreCase("World of Aves");
		}
		else if (splitted[0].equalsIgnoreCase("Snake")) {
			return splitted[1].equalsIgnoreCase("reptiles") && splitted[2].equalsIgnoreCase("Reptillian Kingdom");
		}
		else {
			return false;
		}
	}

	public long countValidRecords() {
		long validRecords = 0;
		for (String record : lines) {
			if (isValid(record)) {
				++validRecords;
			}
		}
		return validRecords;
	}

	public long countInvalidRecords() {
		return lines.size() - countValidRecords();
	}
}