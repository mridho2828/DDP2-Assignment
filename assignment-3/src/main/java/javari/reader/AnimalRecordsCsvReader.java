package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;
import javari.animal.*;

public class AnimalRecordsCsvReader extends CsvReader {

	private ArrayList<Integer> uniqID = new ArrayList<Integer>();

	public AnimalRecordsCsvReader(Path file) throws IOException {
		super(file);
	}

	private boolean findID(int i) {
		for (int id : uniqID) {
			if (id == i) {
				return true;
			}
		}
		return false;
	}

	private boolean isValid(String str) {
		String splitted[] = str.split(COMMA);
		if (splitted.length != 8) return false;
		int id = -1;
		for (int i = 0; i < 8; ++i) {
			switch (i) {
				case 0:	// unique id
				try {
					id = Integer.parseInt(splitted[i]);
					if (findID(id)) return false;
				}
				catch (Exception e) {
					return false;
				}
				case 1:	// tipe animal
				if (!Animal.isInThePark(splitted[i])) {
					return false;
				}
				case 3: // gender
				try {
					Gender.parseGender(splitted[i]);
				}
				catch (UnsupportedOperationException e) {
					return false;
				}
				case 4:	// weight
				try {
					Double.parseDouble(splitted[i]);
				}
				catch (Exception e) {
					return false;
				}
				case 5: // length
				try {
					Double.parseDouble(splitted[i]);
				}
				catch (Exception e) {
					return false;
				}
				case 7:
				try {
					Condition.parseCondition(splitted[i]);
				}
				catch (UnsupportedOperationException e) {
					return false;
				}
			}
		}
		uniqID.add(id);
		return true;
	}

	public long countValidRecords() {
		long validRecords = 0;
		for (String record : lines) {
			if (isValid(record)) {
				++validRecords;
			}
		}
		return validRecords;
	}

	public long countInvalidRecords() {
		return lines.size() - countValidRecords();
	}
}