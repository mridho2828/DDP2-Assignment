package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;
import javari.animal.aves.*;
import javari.animal.mammal.*;
import javari.animal.reptile.*;

public class AnimalAttractionsCsvReader extends CsvReader {

	public AnimalAttractionsCsvReader(Path file) throws IOException {
		super(file);
	}

    private static boolean findAttractions(String str, String[] validAttractions) {
        for (String attraction : validAttractions) {
            if (attraction.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

	private boolean isValid(String str) {
		String splitted[] = str.split(",");
		if (splitted.length != 2) return false;
		if (splitted[0].equalsIgnoreCase("Parrot")) {
			return findAttractions(splitted[1], Parrot.getValidAttractions());
		}
		else if (splitted[0].equalsIgnoreCase("Eagle")) {
			return findAttractions(splitted[1], Eagle.getValidAttractions());
		}
		else if (splitted[0].equalsIgnoreCase("Snake")) {
			return findAttractions(splitted[1], Snake.getValidAttractions());
		}
		else if (splitted[0].equalsIgnoreCase("Cat")) {
			return findAttractions(splitted[1], Cat.getValidAttractions());
		}
		else if (splitted[0].equalsIgnoreCase("Lion")) {
			return findAttractions(splitted[1], Lion.getValidAttractions());
		}
		else if (splitted[0].equalsIgnoreCase("Hamster")) {
			return findAttractions(splitted[1], Hamster.getValidAttractions());
		}
		else if (splitted[0].equalsIgnoreCase("Whale")) {
			return findAttractions(splitted[1], Whale.getValidAttractions());
		}
		else {
			return false;
		}
	}

	public long countValidRecords() {
		long validRecords = 0;
		for (String record : lines) {
			if (isValid(record)) {
				++validRecords;
			}
		}
		return validRecords;
	}

	public long countInvalidRecords() {
		return lines.size() - countValidRecords();
	}
}