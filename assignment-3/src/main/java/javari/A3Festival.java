package javari;

import javari.animal.*;
import javari.animal.aves.*;
import javari.animal.mammal.*;
import javari.animal.reptile.*;
import javari.reader.*;
import javari.park.*;
import javari.writer.*;
import java.util.Scanner;
import java.nio.file.Paths;

public class A3Festival {

	private static String ATTRACTIONS = "animal_attractions.csv";
	private static String CATEGORIES = "animal_categories.csv";
	private static String RECORDS = "animal_records.csv";
	private static Scanner input = new Scanner(System.in);

	public static Registration getRegistration() {
		System.out.println("Welcome to Javari Park Festival - Registration Service!");
		System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");
		SelectedAttraction attraction = null;
		while (attraction == null) {
			System.out.println("Javari Park has 3 sections:");
			System.out.println("1. Explore the Mammals");
			System.out.println("2. World of Aves");
			System.out.println("3. Reptilian Kingdom");
			String response = input.nextLine();
			if (input.equals("1")) attraction = Mammal.mammalInterface();
			else if (input.equals("2")) attraction = Aves.avesInterface();
			else attraction = Reptile.reptileInterface();
		}
	}

	public static void main(String[] args) {
		System.out.println("Welcome to Javari Park Festival - Registration Service!");
		System.out.println();
		System.out.print("Please provide the source data path: ");
		String dir = input.nextLine();
		try {
			CsvReader csvAttractions = new AnimalAttractionsCsvReader(Paths.get(dir + ATTRACTIONS));
			CsvReader csvCategories = new AnimalCategoriesCsvReader(Paths.get(dir + CATEGORIES));
			CsvReader csvRecords = new AnimalRecordsCsvReader(Paths.get(dir + RECORDS));
			System.out.println("... Loading... Success... System is populating data...");
		}
		catch (Exception e) {
			System.out.println("... Opening default section database from data. ... File not found or incorrect file!");
		}

		boolean finish = false;
		while (!finish) {
			Registration regis = getRegistration();
			try {
				RegistrationWriter.writeJson(regis, Paths.get(dir));
			}
			catch (Exception e) {

			}
			System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
			String response = input.nextLine();
			if (response.equalsIgnoreCase("N")) {
				finish = true;
			}
		}
	}
}