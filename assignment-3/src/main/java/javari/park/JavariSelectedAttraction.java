package javari.park;

import java.util.List;
import java.util.ArrayList;
import javari.animal.*;

public class JavariSelectedAttraction implements SelectedAttraction {

	private String name;
	private String type;
	private ArrayList<Animal> performers;

    public JavariSelectedAttraction(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
    	return name;
    }

    public String getType() {
    	return type;
    }

    public List<Animal> getPerformers() {
    	return performers;
    }

    public boolean addPerformer(Animal performer) {
    	performers.add(performer);
    	return true;
    }
}