	package javari.park;

import java.util.List;
import java.util.ArrayList;
import javari.*;

public class JavariRegistration implements Registration {

	private String name;
	private int id;
	private static int currentId = 0;
	private ArrayList<SelectedAttraction> selected = new ArrayList<SelectedAttraction>();

	public JavariRegistration(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public int getRegistrationId() {
		return id;
	}

	public String getVisitorName() {
		return name;
	}

	public String setVisitorName(String name) {
		this.name = name;
		this.id = ++currentId;
		return this.name;
	}

	public List<SelectedAttraction> getSelectedAttractions() {
		return selected;
	}

	public boolean addSelectedAttraction(SelectedAttraction selected) {
		this.selected.add(selected);
		return true;
	}
}