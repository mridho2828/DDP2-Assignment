package javari.animal.aves;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import java.util.Scanner;

public abstract class Aves extends Animal {

    protected boolean isLayingEggs;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    protected abstract boolean specificCondition();

    public static SelectedAttraction avesInterface() {
    	System.out.println("--World of Aves--");
    	System.out.println("1. Eagle");
    	System.out.println("2. Parrot");
    	System.out.println("Please choose your preferred animals (type the number): ");
    	Scanner input = new Scanner(System.in);
        String response = input.nextLine();
        if (response.equals("1")) return Eagle.eagleInterface();
        else if (response.equals("2")) return Parrot.parrotInterface();
        else return null;
    }
}