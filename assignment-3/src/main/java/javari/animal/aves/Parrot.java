package javari.animal.aves;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;

public class Parrot extends Aves {

    private static String[] validAttractions = {
        "Dancing Animals",
        "Counting Masters"
    };

    public static String[] getValidAttractions() {
        return validAttractions;
    }

    public Parrot(Integer id, String type, String name, String gender, double length,
                double weight, String specificCondition, String condition) {

    	super(id, type, name,
    		Gender.parseGender(gender),
    		length, weight,
    		Condition.parseCondition(condition)
    	);
        if (specificCondition.length() == 0) this.isLayingEggs = false;
        else this.isLayingEggs = specificCondition.equalsIgnoreCase("laying eggs");
    }

    protected boolean specificCondition() {
    	return !isLayingEggs;
    }

    public static SelectedAttraction parrotInterface() {
        System.out.println("---Parrot---");
        System.out.println("Attractions by Parrot");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Counting Masters");
        System.out.println("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        if (input.equals("1")) return new JavariSelectedAttraction("Dancing Animals", "Eagle");
        else if (input.equals("2")) return new JavariSelectedAttraction("Counting Masters", "Eagle");
        else return null;
    }
}