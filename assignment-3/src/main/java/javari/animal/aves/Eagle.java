package javari.animal.aves;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;


public class Eagle extends Aves {

    private static String[] validAttractions = {
        "Circle of Fire"
    };

    public static String[] getValidAttractions() {
        return validAttractions;
    }

    public Eagle(Integer id, String type, String name, String gender, double length,
                double weight, String specificCondition, String condition) {

    	super(id, type, name,
    		Gender.parseGender(gender),
    		length, weight,
    		Condition.parseCondition(condition)
    	);
        if (specificCondition.length() == 0) this.isLayingEggs = false;
    	else this.isLayingEggs = specificCondition.equalsIgnoreCase("laying eggs");
    }

    protected boolean specificCondition() {
    	return !isLayingEggs;
    }

    public static SelectedAttraction eagleInterface() {
        System.out.println("---Eagle---");
        System.out.println("Attractions by Eagle");
        System.out.println("1. Circle of Fire");
        System.out.println("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        if (input.equals("1")) return new JavariSelectedAttraction("Circle of Fire", "Eagle");
        else return null;
    }
}