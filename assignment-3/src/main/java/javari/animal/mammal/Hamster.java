package javari.animal.mammal;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;

public class Hamster extends Mammal {

    private static String[] validAttractions = {
        "Dancing Animals",
        "Counting Masters",
        "Passionate Coders",
    };

    public static String[] getValidAttractions() {
        return validAttractions;
    }

	public Hamster(Integer id, String type, String name, String gender, double length,
                double weight, String specificCondition, String condition) {

		super(id, type, name,
			Gender.parseGender(gender),
			length, weight,
			Condition.parseCondition(condition)
		);
		if (specificCondition.length() == 0) this.isPregnant = false;
		else this.isPregnant = specificCondition.equalsIgnoreCase("pregnant");
	}

	protected boolean specificCondition() {
		return !isPregnant;
	}

    public static SelectedAttraction hamsterInterface() {
        System.out.println("---Hamster---");
        System.out.println("Attractions by Hamster");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Counting Masters");
        System.out.println("3. Passionate Coders");
        System.out.println("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        if (input.equals("1")) return new JavariSelectedAttraction("Dancing Animals", "Hamster");
        else if (input.equals("2")) return new JavariSelectedAttraction("Counting Masters", "Hamster");
        else if (input.equals("3")) return new JavariSelectedAttraction("Passionate Coders", "Hamster");
        else return null;
    }
}