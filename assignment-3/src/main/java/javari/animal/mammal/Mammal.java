package javari.animal.mammal;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;

public abstract class Mammal extends Animal {

    protected boolean isPregnant;
    
    public Mammal(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    protected abstract boolean specificCondition();

    public static SelectedAttraction mammalInterface() {
    	System.out.println("--Explore the Mammals--");
    	System.out.println("1. Hamster");
    	System.out.println("2. Lion");
    	System.out.println("3. Cat");
    	System.out.println("4. Whale");
    	System.out.println("Please choose your preferred animals (type the number): ");
    	Scanner input = new Scanner(System.in);
        String response = input.nextLine();
        if (response.equals("1")) return Hamster.hamsterInterface();
        else if (response.equals("2")) return Lion.lionInterface();
        else if (response.equals("3")) return Cat.catInterface();
        else if (response.equals("4")) return Whale.whaleInterface();
        else return null;
    }
}