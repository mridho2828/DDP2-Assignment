package javari.animal.mammal;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;

public class Whale extends Mammal {

    private static String[] validAttractions = {
        "Circle of Fire",
        "Counting Masters"
    };

    public static String[] getValidAttractions() {
        return validAttractions;
    }

	public Whale(Integer id, String type, String name, String gender, double length,
                double weight, String specificCondition, String condition) {

		super(id, type, name,
			Gender.parseGender(gender),
			length, weight,
			Condition.parseCondition(condition)
		);
		if (specificCondition.length() == 0) this.isPregnant = false;
		else this.isPregnant = specificCondition.equalsIgnoreCase("pregnant");
	}

	protected boolean specificCondition() {
		return !isPregnant;
	}

    public static SelectedAttraction whaleInterface() {
        System.out.println("---Whale---");
        System.out.println("Attractions by Whale");
        System.out.println("1. Circle of Fire");
        System.out.println("2. Counting Masters");
        System.out.println("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        if (input.equals("1")) return new JavariSelectedAttraction("Circle of Fire", "Whale");
        else if (input.equals("2")) return new JavariSelectedAttraction("Counting Masters", "Whale");
        else return null;
    }
}