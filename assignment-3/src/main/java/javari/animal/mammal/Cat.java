package javari.animal.mammal;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;

public class Cat extends Mammal {

    private static String[] validAttractions = {
        "Dancing Animals",
        "Passionate Coders"
    };

    public static String[] getValidAttractions() {
        return validAttractions;
    }

	public Cat(Integer id, String type, String name, String gender, double length,
                double weight, String specificCondition, String condition) {

		super(id, type, name,
			Gender.parseGender(gender),
			length, weight,
			Condition.parseCondition(condition)
		);
		if (specificCondition.length() == 0) this.isPregnant = false;
		else this.isPregnant = specificCondition.equalsIgnoreCase("pregnant");
	}

	protected boolean specificCondition() {
		return !isPregnant;
	}

    public static SelectedAttraction catInterface() {
        System.out.println("---Cat---");
        System.out.println("Attractions by Cat");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Passionate Coders");
        System.out.println("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        if (input.equals("1")) return new JavariSelectedAttraction("Dancing Animals", "Cat");
        else if (input.equals("2")) return new JavariSelectedAttraction("Passionate Coders", "Cat");
        else return null;
    }
}