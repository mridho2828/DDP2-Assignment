package javari.animal.reptile;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import java.util.Scanner;

public abstract class Reptile extends Animal {

    protected boolean isTame;
    
    public Reptile(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    protected abstract boolean specificCondition();

    public static SelectedAttraction reptileInterface() {
    	System.out.println("--Reptilian Kingdom--");
    	System.out.println("1. Snake");
    	System.out.println("Please choose your preferred animals (type the number): ");
    	Scanner input = new Scanner(System.in);
        String response = input.nextLine();
        if (response.equals("1")) return Snake.snakeInterface();
        else return null;
    }
}