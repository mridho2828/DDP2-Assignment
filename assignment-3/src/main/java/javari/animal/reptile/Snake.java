package javari.animal.reptile;

import javari.animal.*;
import javari.park.*;
import java.util.Scanner;

public class Snake extends Reptile {

    private static String[] validAttractions = {
        "Dancing Animals",
        "Passionate Coders"
    };

    public static String[] getValidAttractions() {
        return validAttractions;
    }

	public Snake(Integer id, String type, String name, String gender, double length,
                double weight, String specificCondition, String condition) {

		super(id, type, name,
			Gender.parseGender(gender),
			length, weight,
			Condition.parseCondition(condition)
		);
		if (specificCondition.length() == 0) this.isTame = true;
		else this.isTame = specificCondition.equalsIgnoreCase("tame");
	}

	protected boolean specificCondition() {
		return isTame;
	}

    public static SelectedAttraction snakeInterface() {
        System.out.println("---Snake---");
        System.out.println("Attractions by Snake");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Passionate Coders");
        System.out.println("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        if (input.equals("1")) return new JavariSelectedAttraction("Dancing Animals", "Snake");
        else if (input.equals("2")) return new JavariSelectedAttraction("Passionate Coders", "Snake");
        else return null;
    }
}