import javax.swing.*;
import java.awt.*;

/**
 * Main Class to run the Matching-Pair Game
 */
public class Game {

    /**
     * main method create board object and play game
     * @param args
     */
	public static void main(String[] args) {
	    Board board = new Board();
        board.setPreferredSize(new Dimension(500,500));
        board.setLocation(500, 250);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.pack();
        board.setVisible(true);
	}
}