import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Class to represent the cards
 * Images at the cards use English Football Club logos from given resource
 */
public class Card extends JButton {

    /**
     * string path to the resource directory
     */
    private static final String PATH_TO_IMAGE = "../../resources/";
    /**
     * image icon that will be displayed if this instance has not been matched yet
     */
    public static final ImageIcon BEFORE_IMAGE_ICON = new ImageIcon(
        new File(PATH_TO_IMAGE + "background/Premier League.png"
        ).getAbsolutePath()
    );
    /**
     * list contains the image icons
     */
    private static ArrayList<File> listImage;
    /**
     * card id of this instance
     */
    private int id;
    /**
     * field to represent whether this instance has (not) been matched
     */
    private boolean isMatched;
    /**
     * image icon of this instance that will be displayed when a user click the card
     */
    private ImageIcon realImageIcon;

    /**
     * get random image icon from the remaining image list
     * after getting the image, the list removes the image from it
     * @return random image icon
     * @throws IndexOutOfBoundsException if there is no image icon in the list
     */
    public static ImageIcon getRandomImageIcon() throws IndexOutOfBoundsException {
        Collections.shuffle(listImage);
        ImageIcon randomImageIcon = new ImageIcon(listImage.get(0).getAbsolutePath());
        listImage.remove(0);
        return randomImageIcon;
    }

    /**
     * reset listImage, after this method is called listImage will have all image icon from the resource
     */
    public static void resetImageIcons() {
        listImage = new ArrayList<File>(
            Arrays.asList(new File(PATH_TO_IMAGE + "logo/").listFiles())
        );
    }

    /**
     * return a scaled object of given image icon
     * @param ic given image icon
     * @return new image icon that has been scaled
     */
    public static ImageIcon getScaledImageIcon(ImageIcon ic) {
        Image img = ic.getImage();
        Image scaledImage = img.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        return new ImageIcon(scaledImage);
    }

    /**
     * Construct an instance of this class
     * @param id id of this instance
     * @param img realImageIcon of this instance
     */
    public Card(int id, ImageIcon img) {
        this.id = id;
        this.isMatched = false;
        this.realImageIcon = img;
        this.setIcon(getScaledImageIcon(BEFORE_IMAGE_ICON));
    }

    /**
     * get the id of this instance
     * @return id of this instance
     */
    public int getID() { return id; }

    /**
     * return true if this instance already matched, false otherwise
     * @return isMatched of this instance
     */
    public boolean isMatched() { return  isMatched; }

    /**
     * set the match status of this instance
     * @param matched
     */
    public void setMatched(boolean matched) { isMatched = matched; }

    /**
     * get the realImageIcon of this instance
     * @return realImageIcon of this instance
     */
    public ImageIcon getRealImageIcon() { return realImageIcon; }

    /**
     * set the realImageIcon of this instance
     * @param realImageIcon
     */
    public void setRealImageIcon(ImageIcon realImageIcon) {
        this.realImageIcon = realImageIcon;
    }
}