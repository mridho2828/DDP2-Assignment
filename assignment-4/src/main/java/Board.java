import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class to display the board of the game and implement all features
 */
public class Board extends JFrame {

    /**
     * number of card pairs
     */
    private final int PAIRS = 18;
    /**
     * title of the game
     */
    private final String TITLE = "TP4 - ENGLISH FOOTBALL CLUB VERSION";
    /**
     * list contains the cards used at thisi nstance
     */
    private ArrayList<Card> cardsList = new ArrayList<Card>();
    /**
     * list contains the image icons used at this instance
     */
    private ArrayList<ImageIcon> imageIconsList = new ArrayList<ImageIcon>();
    /**
     * card selected at the first time
     */
    private Card c1;
    /**
     * card selected at the second time
     */
    private Card c2;
    /**
     * timer to give time for user to memorize the selected cards
     */
    private Timer t;
    /**
     * count the attempts of the user from the beginning
     */
    private int attemptsCounter;
    /**
     * highscore of this instance
     */
    private int highscore;
    /**
     * if the value is true, user can't select cards
     */
    private boolean isDelay;
    /**
     * container to contain all panel
     */
    private Container container;
    /**
     * panel to represent grid of the cards
     */
    private JPanel gridPanel;
    /**
     * panel to represent reset button and exit button
     */
    private JPanel panel1;
    /**
     * panel to represent attempt counter label
     */
    private JPanel panel2;
    /**
     * panel to represent highscore label
     */
    private JPanel panel3;
    /**
     * this instance will restart the game when this button is clicked
     */
    private JButton resetButton;
    /**
     * close the system when this button is clicked
     */
    private JButton exitButton;
    /**
     * label to represent attempt counter value
     */
    private JLabel attemptsLabel;
    /**
     * label to represent highscore value
     */
    private JLabel highscoreLabel;

    /**
     * Construct new instance with all features
     */
    public Board() {
        this.setTitle(TITLE);
        container = new Container();
        gridPanel = new JPanel();
        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();
        attemptsLabel = new JLabel();
        highscoreLabel = new JLabel();
        initialize();
        addCards();
        addButtons();
        addPanels();
        addContainer();
        addTimer();
    }

    /**
     * initialize values of the instance's field (except for containers, panels, and buttons)
     */
    private void initialize() {
        attemptsCounter = 0;
        attemptsLabel.setText("Number of attempt(s): " + attemptsCounter);
        highscoreLabel.setText("Current highscore: " + highscore);
        Card.resetImageIcons();
        c1 = null;
        c2 = null;
        isDelay = false;
        imageIconsList.clear();
        for (int i = 1; i <= PAIRS; ++i) {
            try {
                imageIconsList.add(Card.getRandomImageIcon());
            }
            catch (IndexOutOfBoundsException e) {
                System.out.println("no image left!");
            }
        }
    }

    /**
     * create new cards object, shuffle them, and place them at gridPanel
     */
    private void addCards() {
        gridPanel.removeAll();
        cardsList.clear();
        for (int i = 1; i <= PAIRS; ++i) {
            for (int j = 1; j <= 2; ++j) {
                Card c = new Card(i, imageIconsList.get(i-1));
                c.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        turn(c);
                    }
                });
                cardsList.add(c);
            }
        }
        Collections.shuffle(cardsList);
        gridPanel.setLayout(new GridLayout(6,6));
        for (Card c : cardsList) {
            gridPanel.add(c);
        }
    }

    /**
     * remove previous cards object, create new ones
     */
    private void resetCards() {
        initialize();
        addCards();
    }

    /**
     * create new resetButton and exitButton objects
     */
    private void addButtons() {
        resetButton = new JButton("RESET");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                resetCards();
            }
        });

        exitButton = new JButton("EXIT");
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });
    }

    /**
     * add panel1, panel2, panel3
     */
    private void addPanels() {
        panel1.setLayout(new FlowLayout());
        panel1.add(resetButton);
        panel1.add(exitButton);

        panel2.setLayout(new FlowLayout());
        panel2.add(attemptsLabel);

        panel3.setLayout(new FlowLayout());
        panel3.add(highscoreLabel);
    }

    /**
     * add container that contains all panels
     */
    private void addContainer() {
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.add(gridPanel);
        container.add(panel1);
        container.add(panel2);
        container.add(panel3);
        this.add(container);
    }

    /**
     * create Timer objects to handle checkCards method so user have time to memorize selected cards,
     * assign it to field t
     */
    private void addTimer() {
        t = new Timer(750, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                checkCards();
                isDelay = false;
            }
        });
        t.setRepeats(false);
    }

    /**
     * check whenever a card is selected and it is not at delayed time
     * @param selectedCard card selected by user
     */
    private void turn(Card selectedCard) {
        if (isDelay) return;
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.setIcon(Card.getScaledImageIcon(c1.getRealImageIcon()));
        }
        else if (c1 != selectedCard) {
            c2 = selectedCard;
            c2.setIcon(Card.getScaledImageIcon(c2.getRealImageIcon()));
            ++attemptsCounter;
            attemptsLabel.setText("Number of attempt(s): " + attemptsCounter);
            isDelay = true;
            t.start();
        }
    }

    /**
     * check whenever two cards have been selected.
     * if the cards' id are the same, they will be inactive.
     * if the game has been won, a confirm dialog will be showed
     */
    private void checkCards() {
        if (c1.getID() == c2.getID()) {
            c1.setEnabled(false);
            c2.setEnabled(false);
            c1.setMatched(true);
            c2.setMatched(true);
            if (isWin()) {
                if (highscore == 0 || attemptsCounter < highscore) {
                    highscore = attemptsCounter;
                    JOptionPane.showConfirmDialog(
                            null, "You got a new highscore!", "New Highscore", JOptionPane.PLAIN_MESSAGE);
                }
                int result = JOptionPane.showConfirmDialog(
                        null, "Play Again?", "Yeay!", JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                   resetCards();
                }
                else {
                    System.exit(0);
                }
            }
        }
        else {
            c1.setIcon(Card.getScaledImageIcon(Card.BEFORE_IMAGE_ICON));
            c2.setIcon(Card.getScaledImageIcon(Card.BEFORE_IMAGE_ICON));
        }
        c1 = null;
        c2 = null;
    }

    /**
     * check whether user has won the game
     * @return true if user has won the game (all cars have been matched), false otherwise
     */
    private boolean isWin() {
        for (Card c : cardsList) {
            if (!c.isMatched()) {
                return false;
            }
        }
        return true;
    }
}