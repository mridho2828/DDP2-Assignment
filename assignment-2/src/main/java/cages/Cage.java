package cages;

public class Cage {

	char type;
	String animalName;
	int animalLength;
	int cageLength;
	int cageWidth;

	public Cage(char type, String animalName, int animalLength, int cageLength, int cageWidth) {
		this.type = type;
		this.animalName = animalName;
		this.animalLength = animalLength;
		this.cageLength = cageLength;
		this.cageWidth = cageWidth;
	}

	public char getType() { return type; }
	public String getAnimalName() { return animalName; }
	public int getAnimalLength() { return animalLength; }
}