package cages;

public class Outdoor extends Cage {

	public Outdoor(String animalName, int animalLength) {
		super(
			getType(animalLength),
			animalName,
			animalLength,
			120,
			getWidth(getType(animalLength))
		);
	}

	public static char getType(int animalLength) {
		if (animalLength < 75) return 'A';
		else if (animalLength <= 90) return 'B';
		else return 'C';
	}

	public static int getWidth(char type) {
		if (type == 'A') return 120;
		else if (type == 'B') return 150;
		else return 180;
	}
}