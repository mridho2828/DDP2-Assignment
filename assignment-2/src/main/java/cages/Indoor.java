package cages;

public class Indoor extends Cage {

	public Indoor(String animalName, int animalLength) {
		super(
			getType(animalLength),
			animalName,
			animalLength,
			60,
			getWidth(getType(animalLength))
		);
	}

	public static char getType(int animalLength) {
		if (animalLength < 45) return 'A';
		else if (animalLength <= 60) return 'B';
		else return 'C';
	}

	public static int getWidth(char type) {
		if (type == 'A') return 60;
		else if (type == 'B') return 90;
		else return 120;
	}
}