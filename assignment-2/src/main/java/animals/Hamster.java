package animals;

import java.util.ArrayList;
import cages.*;
import arrangements.*;

public class Hamster extends Animal {

	private Indoor cage;
	private static ArrayList<Hamster> listOfHamsters = new ArrayList<Hamster>();
	private static Cage[][] level = new Cage[3][];

	public Hamster(String name, int length) {
		super(name, length);
		this.cage = new Indoor(name, length);
		listOfHamsters.add(this);
	}

	public Cage getCage() { return cage; }
	public static int getSize() { return listOfHamsters.size(); }

	private static void init() { level = Arrangement.init(listOfHamsters); }
	private static void reverse() { level = Arrangement.reverse(level); }

	private static void printLevel() {
		for (int i = 2; i >= 0; --i) {
			System.out.format("level %d:", i+1);
			for (int j = 0; j < level[i].length; ++j) {
				System.out.format(
					" %s (%d - %c),",
					level[i][j].getAnimalName(),
					level[i][j].getAnimalLength(),
					level[i][j].getType()
				);
			}
			System.out.println();
		}
	}

	public static void printArrangement() {
		if (listOfHamsters.size() == 0) return;
		System.out.println("location: indoor");
		init();
		printLevel();
		System.out.println("\nAfter rearrangement...");
		reverse();
		printLevel();
		System.out.println();
	}

	public static Hamster getHamster(String name) {
		for (Hamster x : listOfHamsters) {
			if (x.name.equals(name)) return x;
		}
		return null;
	}

	public void gnaw() {
		System.out.format("%s makes a voice: ngkkrit.. ngkkrrriiit\n", name);
	}

	public void run() {
		System.out.format("%s makes a voice: trrr…. trrr...\n", name);
	}
}