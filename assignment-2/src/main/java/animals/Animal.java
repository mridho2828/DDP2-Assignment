package animals;

import cages.*;

public abstract class Animal {

	String name;
	int length;

	public Animal(String name, int length) {
		this.name = name;
		this.length = length;
	}

	public abstract Cage getCage();
}