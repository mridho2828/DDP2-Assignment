package animals;

import java.util.ArrayList;
import java.util.Random;
import cages.*;
import arrangements.*;

public class Cat extends Animal {

	private Indoor cage;
	private static ArrayList<Cat> listOfCats = new ArrayList<Cat>();
	private static Cage[][] level = new Cage[3][];

	public Cat(String name, int length) {
		super(name, length);
		this.cage = new Indoor(name, length);
		listOfCats.add(this);
	}

	public Cage getCage() { return cage; }
	public static int getSize() { return listOfCats.size(); }

	private static void init() { level = Arrangement.init(listOfCats); }
	private static void reverse() { level = Arrangement.reverse(level); }

	private static void printLevel() {
		for (int i = 2; i >= 0; --i) {
			System.out.format("level %d:", i+1);
			for (int j = 0; j < level[i].length; ++j) {
				System.out.format(
					" %s (%d - %c),",
					level[i][j].getAnimalName(),
					level[i][j].getAnimalLength(),
					level[i][j].getType()
				);
			}
			System.out.println();
		}
	}

	public static void printArrangement() {
		if (listOfCats.size() == 0) return;
		System.out.println("location: indoor");
		init();
		printLevel();
		System.out.println("\nAfter rearrangement...");
		reverse();
		printLevel();
		System.out.println();
	}

	public static Cat getCat(String name) {
		for (Cat x : listOfCats) {
			if (x.name.equals(name)) return x;
		}
		return null;
	}

	public void brushFur() {
		System.out.format("Time to clean %s's fur\n", name);
		System.out.format("%s makes a voice: Nyaaan...\n", name);
	}

	public void cuddle() {
		Random rand = new Random();
		int value = rand.nextInt(4);
		if (value == 0) System.out.format("%s makes a voice: Miaaaw..\n", name);
		else if (value == 1) System.out.format("%s makes a voice: Purrr..\n", name);
		else if (value == 2) System.out.format("%s makes a voice: Mwaw!\n", name);
		else System.out.format("%s makes a voice: Mraaawr!\n", name);
	}
}