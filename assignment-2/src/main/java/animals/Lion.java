package animals;

import java.util.ArrayList;
import cages.*;
import arrangements.*;

public class Lion extends Animal {
	
	private Outdoor cage;
	private static ArrayList<Lion> listOfLions = new ArrayList<Lion>();
	private static Cage[][] level = new Cage[3][];

	public Lion(String name, int length) {
		super(name, length);
		this.cage = new Outdoor(name, length);
		listOfLions.add(this);
	}

	public Cage getCage() { return cage; }
	public static int getSize() { return listOfLions.size(); }

	private static void init() { level = Arrangement.init(listOfLions); }
	private static void reverse() { level = Arrangement.reverse(level); }

	private static void printLevel() {
		for (int i = 2; i >= 0; --i) {
			System.out.format("level %d:", i+1);
			for (int j = 0; j < level[i].length; ++j) {
				System.out.format(
					" %s (%d - %c),",
					level[i][j].getAnimalName(),
					level[i][j].getAnimalLength(),
					level[i][j].getType()
				);
			}
			System.out.println();
		}
	}

	public static void printArrangement() {
		if (listOfLions.size() == 0) return;
		System.out.println("location: outdoor");
		init();
		printLevel();
		System.out.println("\nAfter rearrangement...");
		reverse();
		printLevel();
		System.out.println();
	}

	public static Lion getLion(String name) {
		for (Lion x : listOfLions) {
			if (x.name.equals(name)) return x;
		}
		return null;
	}

	public void hunt() {
		System.out.println("Lion is hunting..");
		System.out.format("%s makes a voice: err...!\n", name);
	}

	public void brushMane() {
		System.out.println("Clean the lion’s mane..");
		System.out.format("%s makes a voice: Hauhhmm!\n", name);
	}

	public void disturb() {
		System.out.format("%s makes a voice: HAUHHMM!\n", name);
	}
}