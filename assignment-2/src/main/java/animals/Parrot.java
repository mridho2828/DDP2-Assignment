package animals;

import java.util.ArrayList;
import java.util.Scanner;
import cages.*;
import arrangements.*;

public class Parrot extends Animal {

	private Indoor cage;
	private static ArrayList<Parrot> listOfParrots = new ArrayList<Parrot>();
	private static Cage[][] level = new Cage[3][];

	public Parrot(String name, int length) {
		super(name, length);
		this.cage = new Indoor(name, length);
		listOfParrots.add(this);
	}

	public Cage getCage() { return cage; }
	public static int getSize() { return listOfParrots.size(); }

	private static void init() { level = Arrangement.init(listOfParrots); }
	private static void reverse() { level = Arrangement.reverse(level); }

	private static void printLevel() {
		for (int i = 2; i >= 0; --i) {
			System.out.format("level %d:", i+1);
			for (int j = 0; j < level[i].length; ++j) {
				System.out.format(
					" %s (%d - %c),",
					level[i][j].getAnimalName(),
					level[i][j].getAnimalLength(),
					level[i][j].getType()
				);
			}
			System.out.println();
		}
	}

	public static void printArrangement() {
		if (listOfParrots.size() == 0) return;
		System.out.println("location: indoor");
		init();
		printLevel();
		System.out.println("\nAfter rearrangement...");
		reverse();
		printLevel();
		System.out.println();
	}

	public static Parrot getParrot(String name) {
		for (Parrot x : listOfParrots) {
			if (x.name.equals(name)) return x;
		}
		return null;
	}

	public void fly() {
		System.out.format("Parrot %s flies!\n", name);
		System.out.format("%s makes a voice: FLYYYY…\n", name);
	}

	public void doConversation() {
		System.out.print("You say: ");
		Scanner input = new Scanner(System.in);
		String sentence = input.nextLine();
		System.out.format("%s says: %s\n", name, sentence.toUpperCase());
	}

	public void doNothing() {
		System.out.format("%s says: HM?\n", name);
	}
}