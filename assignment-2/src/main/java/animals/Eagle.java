package animals;

import java.util.ArrayList;
import cages.*;
import arrangements.*;

public class Eagle extends Animal {
	
	private Outdoor cage;
	private static ArrayList<Eagle> listOfEagles = new ArrayList<Eagle>();
	private static Cage[][] level = new Cage[3][];

	public Eagle(String name, int length) {
		super(name, length);
		this.cage = new Outdoor(name, length);
		listOfEagles.add(this);
	}

	public Cage getCage() { return cage; }
	public static int getSize() { return listOfEagles.size(); }

	private static void init() { level = Arrangement.init(listOfEagles); }
	private static void reverse() { level = Arrangement.reverse(level); }

	private static void printLevel() {
		for (int i = 2; i >= 0; --i) {
			System.out.format("level %d:", i+1);
			for (int j = 0; j < level[i].length; ++j) {
				System.out.format(
					" %s (%d - %c),",
					level[i][j].getAnimalName(),
					level[i][j].getAnimalLength(),
					level[i][j].getType()
				);
			}
			System.out.println();
		}
	}

	public static void printArrangement() {
		if (listOfEagles.size() == 0) return;
		System.out.println("location: outdoor");
		init();
		printLevel();
		System.out.println("\nAfter rearrangement...");
		reverse();
		printLevel();
		System.out.println();
	}

	public static Eagle getEagle(String name) {
		for (Eagle x : listOfEagles) {
			if (x.name.equals(name)) return x;
		}
		return null;
	}

	public void fly() {
		System.out.format("%s makes a voice: kwaakk…\n", name);
		System.out.println("You hurt!");
	}
}