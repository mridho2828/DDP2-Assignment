import java.util.Scanner;
import animals.*;
import cages.*;
import arrangements.*;

public class MainProgram {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals");

		// CAT
		System.out.print("cat: ");
		int n = Integer.parseInt(input.nextLine());
		if (n > 0) {
			System.out.println("Provide the information of cat(s):");
			String[] splitted = input.nextLine().split(",");
			for (int i = 0; i < splitted.length; ++i) {
				String name = splitted[i].split("\\|")[0];
				int length = Integer.parseInt(splitted[i].split("\\|")[1]);
				new Cat(name, length);
			}
		}
		// LION
		System.out.print("lion: ");
		n = Integer.parseInt(input.nextLine());
		if (n > 0) {
			System.out.println("Provide the information of lion(s):");
			String[] splitted = input.nextLine().split(",");
			for (int i = 0; i < splitted.length; ++i) {
				String name = splitted[i].split("\\|")[0];
				int length = Integer.parseInt(splitted[i].split("\\|")[1]);
				new Lion(name, length);
			}
		}
		// EAGLE
		System.out.print("eagle: ");
		n = Integer.parseInt(input.nextLine());
		if (n > 0) {
			System.out.println("Provide the information of eagle(s):");
			String[] splitted = input.nextLine().split(",");
			for (int i = 0; i < splitted.length; ++i) {
				String name = splitted[i].split("\\|")[0];
				int length = Integer.parseInt(splitted[i].split("\\|")[1]);
				new Eagle(name, length);
			}
		}
		// PARROT
		System.out.print("parrot: ");
		n = Integer.parseInt(input.nextLine());
		if (n > 0) {
			System.out.println("Provide the information of parrot(s):");
			String[] splitted = input.nextLine().split(",");
			for (int i = 0; i < splitted.length; ++i) {
				String name = splitted[i].split("\\|")[0];
				int length = Integer.parseInt(splitted[i].split("\\|")[1]);
				new Parrot(name, length);
			}
		} 
		// HAMSTER
		System.out.print("hamster: ");
		n = Integer.parseInt(input.nextLine());
		if (n > 0) {
			System.out.println("Provide the information of hamster(s):");
			String[] splitted = input.nextLine().split(",");
			for (int i = 0; i < splitted.length; ++i) {
				String name = splitted[i].split("\\|")[0];
				int length = Integer.parseInt(splitted[i].split("\\|")[1]);
				new Hamster(name, length);
			}
		}

		System.out.println("Animals have been successfully recorded!\n");
		System.out.println("=============================================");

		System.out.println("Cage arrangement:");
		Cat.printArrangement();
		Lion.printArrangement();
		Eagle.printArrangement();
		Parrot.printArrangement();
		Hamster.printArrangement();

		System.out.println("NUMBER OF ANIMALS:");
		System.out.println("cat:" + Cat.getSize());
		System.out.println("lion:" + Lion.getSize());
		System.out.println("parrot:" + Parrot.getSize());
		System.out.println("eagle:" + Eagle.getSize());
		System.out.println("hamster:" + Hamster.getSize());

		System.out.println("\n=============================================");

		while (true) {
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
			int command = Integer.parseInt(input.nextLine());
			if (command == 1) {
				System.out.print("Mention the name of cat you want to visit: ");
				String name = input.nextLine();
				Cat x = Cat.getCat(name);
				if (x == null) System.out.print("There is no cat with that name! ");
				else {
					System.out.format("You are visiting %s (cat) now, what would you like to do?\n", name);
					System.out.println("1: Brush the fur 2: Cuddle");
					int activity = Integer.parseInt(input.nextLine());
					if (activity == 1) x.brushFur();
					else if (activity == 2) x.cuddle();
					else System.out.println("You do nothing...");
				}
			}
			else if (command == 2) {
				System.out.print("Mention the name of eagle you want to visit: ");
				String name = input.nextLine();
				Eagle x = Eagle.getEagle(name);
				if (x == null) System.out.print("There is no eagle with that name! ");
				else {
					System.out.format("You are visiting %s (eagle) now, what would you like to do?\n", name);
					System.out.println("1: Order to fly");
					int activity = Integer.parseInt(input.nextLine());
					if (activity == 1) x.fly();
					else System.out.println("You do nothing...");
				}
			}
			else if (command == 3) {
				System.out.print("Mention the name of hamster you want to visit: ");
				String name = input.nextLine();
				Hamster x = Hamster.getHamster(name);
				if (x == null) System.out.print("There is no hamster with that name! ");
				else {
					System.out.format("You are visiting %s (hamster) now, what would you like to do?\n", name);
					System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
					int activity = Integer.parseInt(input.nextLine());
					if (activity == 1) x.gnaw();
					else if (activity == 2) x.run();
					else System.out.println("You do nothing...");
				}
			}
			else if (command == 4) {
				System.out.print("Mention the name of parrot you want to visit: ");
				String name = input.nextLine();
				Parrot x = Parrot.getParrot(name);
				if (x == null) System.out.print("There is no parrot with that name! ");
				else {
					System.out.format("You are visiting %s (parrot) now, what would you like to do?\n", name);
					System.out.println("1: Order to fly 2: Do conversation");
					int activity = Integer.parseInt(input.nextLine());
					if (activity == 1) x.fly();
					else if (activity == 2) x.doConversation();
					else x.doNothing();
				}
			}
			else if (command == 5) {
				System.out.print("Mention the name of lion you want to visit: ");
				String name = input.nextLine();
				Lion x = Lion.getLion(name);
				if (x == null) System.out.print("There is no lion with that name! ");
				else {
					System.out.format("You are visiting %s (lion) now, what would you like to do?\n", name);
					System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
					int activity = Integer.parseInt(input.nextLine());
					if (activity == 1) x.hunt();
					else if (activity == 2) x.brushMane();
					else if (activity == 3) x.disturb();
					else System.out.println("You do nothing...");
				}
			}
			else if (command == 99) {
				break;
			}
			else {
				System.out.println("TRY AGAIN!\n");
				continue;
			}
			System.out.println("Back to the office!\n");
		}
		input.close();
	}
}