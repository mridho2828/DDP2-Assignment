package arrangements;

import java.util.ArrayList;
import animals.*;
import cages.*;

public class Arrangement {
	
	public static Cage[][] init(ArrayList <? extends Animal> list) {
		int currentIndex = 0;
		Cage[][] cage = new Cage[3][];
		for (int i = 0; i < 3; ++i) {
			if (i == 0) {
				cage[i] = new Cage[list.size() / 3];
			}
			else if (i == 1) {
				if (list.size() % 3 == 2) cage[i] = new Cage[list.size() / 3 + 1];
				else cage[i] = new Cage[list.size() / 3];
			}
			else {
				if (list.size() % 3 >= 1) cage[i] = new Cage[list.size() / 3 + 1];
				else cage[i] = new Cage[list.size() / 3];				
			}

			for (int j = 0; j < cage[i].length; ++j) {
				cage[i][j] = list.get(currentIndex++).getCage();
			}
		}
		return cage;
	}

	public static Cage[][] reverse(Cage[][] oldCage) {
		Cage[][] newCage = new Cage[3][];
		for (int i = 0; i < 3; ++i) {
			if (i > 0) newCage[i] = new Cage[oldCage[i-1].length];
			else newCage[i] = new Cage[oldCage[2].length];
			for (int j = 0; j < newCage[i].length; ++j) {
				if (i > 0) newCage[i][j] = oldCage[i-1][newCage[i].length - 1 - j];
				else newCage[i][j] = oldCage[2][newCage[i].length - 1 - j];
			}
		}
		return newCage;
	}
}